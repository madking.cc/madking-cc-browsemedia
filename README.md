Stream mp3 files from remote machine over http or https

Extrem simple gui

Details: https://madking.cc/php-software-rex-browsemedia/

VERSION 1.0.0 (17. February 2018, 20:18)

VERSION 1.1.0 (19. February 2018, 22:50)
- Added visual HTML5 Audio Player
- Fixed Bug: Audio play stops after 1 Album
- Added Next Button

VERSION 1.1.1 (24. February 2018, 9:36)
- Added ogg and wav audio support
- Added session destroy button
- Added current filename and filesize
- Fixed Bug if mediaroot path changes and database has to delete old entries

VERSION 1.1.2 (24. February 2018, 10:57)
- Added internal Sorting Algorithm to prevent buggy display of directory structure

VERSION 1.1.3 (24. February 2018, 22:40)
- Small performance enhancements

VERSION 1.2.0 (26. March 2018, 23:13)
- New Look
- Fixed Database Encoding
