<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#USE PHP CLI for this file

if ( ! defined( "rex_browsemedia" ) ) {
	define( "rex_browsemedia", 1 );
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );
require_once( $dir_root . "class.php" );
$template_type = "full";
require_once( $dir_root . "template-elements.php" );


$content           = "";
$c_rex_browsemedia = new rex_browsemedia( $config );
if ( ! isset( $_SESSION["parent"] ) ) {
	$_SESSION["parent"] = 0;
}
$collection = $c_rex_browsemedia->get( $_SESSION["parent"] );

$content .= "<div id='wrapper_content'>";
$content .= "<div class='wrappercollection'>";
$content .= $c_rex_browsemedia->render( $collection );
$content .= "<div class='row element-link-custom'><div class='col-sm-12'><a class='' href='browser.php?destroy=1'>Destroy Session</a></div></div>";
$content .= "</div></div>";
$content_output = $content_header . "
<body>
$content
" . $content_footer;

echo $content_output;

