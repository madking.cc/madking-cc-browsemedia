<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	die();
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );

if ( ! isset( $template_type ) ) {
	$template_type = "full";
}

switch ( $template_type ) {

	///////////
	/// /jquery ONLY
	/// //////////

	case "jquery":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">

<title>Player</title>

<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\" crossorigin=\"anonymous\"></script>
<link rel=\"stylesheet\" href=\"./style.css\">

<style>
	body { background: #010101; color: #fefefe; font-size: 120%; }
</style>
  </head>
";

		$content_footer = "</body></html>";
		break;




	////////////77
	/// / NOCODE
	/// ////////////

	case "nocode":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">
<link rel=\"icon\" type=\"image/png\" href=\"./favicon.png\" />
<title>rex-browsemedia</title>
<link rel=\"stylesheet\" href=\"./style.css\">
  </head>
";

		$content_footer = "</body></html>";
		break;

	/////////////77
	/// ///////FULL
	/// //////////////

	case "full":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">

<title>Browser</title>

<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
<script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>

<script src=\"./ajax.js\"></script>  
<link rel=\"stylesheet\" href=\"./style.css\">
  </head>
";

		$content_footer = "</body></html>";
		break;
	default:
		$content_header = "";
		$content_footer = "";
		break;
}
