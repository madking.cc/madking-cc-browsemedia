<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	die();
}

if ( ! isset( $_SESSION ) ) {
	session_start();
}

if(isset($_GET) && !empty($_GET))
{
	if(isset($_GET["destroy"]) && $_GET["destroy"] == 1)
	{
		session_destroy();
		echo "<!DOCTYPE html>";
		echo "<head>";
		echo "<title>Form submitted</title>";
		echo "<script type='text/javascript'>window.parent.location = window.parent.location.href.split(\"?\")[0];</script>";
		echo "</head>";
		echo "<body></body></html>";
		die();
	}
}

$config = array(
	"mediaroot"               => "/datacenter/Musik/",  // Path to your mp3 Files. Windows Example: "c:\media\audio\"
	"mysql_database"          => "rex_browsemedia",  // The Database Name
	"mysql_database_user"     => "rex_browsemedia", // The Database User
	"mysql_database_password" => "simple",  // The Database User Password
	"mysql_host"              => "localhost",    // No need to change
	"mysql_port"              => 3306,   // No need to change
	"mysql_table_name_data"   => "data",  // No need to change
	"filetypes"               => array( "mp3", "ogg", "wav" ), // Don't change it, (mayb3 in development)
);

if ( ! defined( "rex_debug" ) ) {
	define( "rex_debug", 1 );
}  // Turn Debug on/off