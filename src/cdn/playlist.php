<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! isset( $_SESSION ) ) {
	session_start();
}

error_reporting( E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED );
ini_set( 'track_errors', '0' );
ini_set( 'display_errors', '0' );

//&& sizeof($_SESSION["playlist"] > 1) // hint for future development
if ( isset( $_SESSION["playlist"] ) && ! empty( $_SESSION["playlist"] ) && is_array( $_SESSION["playlist"] ) ) {
	echo json_encode( [ "dataset" => $_SESSION["playlist"][0] ] );
} else {
	echo json_encode( [ "notice" => "nothing" ] );
}
