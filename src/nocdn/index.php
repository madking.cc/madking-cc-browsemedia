<?php /*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	define( "rex_browsemedia", 1 );
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );
require_once( $dir_root . "class.php" );
$content = "";

$template_type = "nocode";
require_once( $dir_root . "template-elements.php" );

$c_rex_browsemedia = new rex_browsemedia( $config );

if ( ! isset( $_SESSION["parent"] ) ) {
	$_SESSION["parent"] = 0;
}

$content = "";

//$style_iframe_player = " style=\"display:none;\"";
//$style_iframe_player = " style='background-color: #fefefe;color:#000000;'";

$content .= "<script>
  function resizeIframePlayer(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  function resizeIframeBrowser(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }  
</script>";

$content_output = $content_header . "
<body>
$content
<div id='wrapper_output_content'>
<iframe style='width: 100%;' id='output_audio' src='player.php' frameborder=\"0\" scrolling=\"no\" onload=\"resizeIframePlayer(this)\"></iframe>
<iframe style='width: 100%;' id='output_content' frameborder=\"0\" scrolling=\"yes\" src='browser.php'></iframe>
</div>
" . $content_footer;

echo $content_output;