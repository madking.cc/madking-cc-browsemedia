/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

// Thanks to: https://stackoverflow.com/a/4313388/8148987
function SetIFrameSource (cid, url) {
  var myframe = parent.document.getElementById(cid)
  if (myframe !== null) {
    if (myframe.src) {
      myframe.src = url
    }
    else if (myframe.contentWindow !== null && myframe.contentWindow.location !== null) {
      myframe.contentWindow.location = url
    }
    else {
      myframe.setAttribute('src', url)
    }
  }
}

function href_id (id) {
  var dataset = {id: id}
  send(dataset)
}

function href_parent (parent) {
  var dataset = {parent: parent}
  send(dataset)
}

function send (dataset) {
  var request = $.ajax({
    url: 'class.php',
    method: 'POST',
    data: dataset,
    dataType: 'json'
  })

  request.done(function (msg) {

    if (msg.hasOwnProperty('error')) {
      alert(msg['error'])
    }
    if (msg.hasOwnProperty('id')) {
      SetIFrameSource('output_audio', 'player.php?id=' + msg['id'])
    }
    if (msg.hasOwnProperty('parent')) {
      window.location.reload(false)
    }

  })

  request.fail(function (jqXHR, textStatus) {
    alert('Request failed: ' + textStatus)
  })
}

