<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	define( "rex_browsemedia", 1 );
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );
require_once( $dir_root . "class.php" );

if ( isset( $_GET ) && ! empty( $_GET ) ) {
	if ( isset( $_GET['id'] ) ) {
		$id = intval( $_GET['id'] );
		if ( ! is_numeric( $id ) || $id < 0 ) {
		} else {
			$c_rex_stream = new rex_stream();

			rex_database::get_instance( $config );

			$path         = rex_database::get_path( $id );
			$c_rex_stream->stream_file_auto( $path );
		}

	}
}
