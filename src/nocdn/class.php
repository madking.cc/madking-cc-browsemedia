<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	define( "rex_browsemedia", 1 );
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );

class rex_stream {

	// Thanks to: https://github.com/henrik242/musicbrowser/

	function stream_mp3( $file ) {

		$this->stream_file( $file, "audio/mpeg" );
	}

	function stream_gif( $file ) {

		$this->stream_file( $file, "image/gif", false );
	}

	function stream_jpeg( $file ) {

		$this->stream_file( $file, "image/jpeg", false );
	}

	function stream_png( $file ) {

		$this->stream_file( $file, "image/png", false );
	}

	function stream_file_auto( $file ) {

		$suffix = strtolower( pathinfo( $file, PATHINFO_EXTENSION ) );

		switch ( $suffix ) {
			case "mp3":
				$this->stream_mp3( $file );
				break;
			case "gif":
				$this->stream_gif( $file );
				break;
			case "png";
				$this->stream_png( $file );
				break;
			case "jpg":
			case "jpeg":
				$this->stream_jpeg( $file );
				break;
			default:
				$this->stream_file( $file, "application/octet-stream" );
				break;
		}
	}

	function stream_file( $file, $mimetype, $isAttachment = true ) {

		$file_array = explode( DIRECTORY_SEPARATOR, $file );
		$filename   = array_pop( $file_array );
		header( "Content-Type: $mimetype" );
		header( "Content-Length: " . filesize( $file ) );
		if ( $isAttachment ) {
			header( "Content-Disposition: attachment; filename=\"$filename\"", true );
		}

		return $this->readfile_chunked( $file );
	}

	function readfile_chunked( $filename, $retbytes = true ) {

		$chunksize = 1 * ( 1024 * 1024 ); // how many bytes per chunk
		$buffer    = "";
		$cnt       = 0;

		$handle = fopen( $filename, "rb" );
		if ( $handle === false ) {
			return false;
		}
		while ( ! feof( $handle ) ) {
			$buffer = fread( $handle, $chunksize );
			echo $buffer;
			@ob_flush();
			flush();
			if ( $retbytes ) {
				$cnt += strlen( $buffer );
			}
		}
		$status = fclose( $handle );
		if ( $retbytes && $status ) {
			return $cnt; // return num. bytes delivered like readfile() does.
		}

		return $status;
	}

	static function get_mime_type( $file ) {

		$suffix = strtolower( pathinfo( $file, PATHINFO_EXTENSION ) );

		switch ( $suffix ) {
			case "mp3":
				return "audio/mpeg";
				break;
			case "gif":
				return "image/gif";
				break;
			case "png";
				return "image/png";
				break;
			case "jpg":
			case "jpeg":
				return "image/jpeg";
				break;
			default:
				return "application/octet-stream";
				break;
		}
	}
}

class rex_browsemedia {

	private $config;

	private $rex_database;

	private $mediaroot_length;

	function __construct( $config ) {

		$this->config           = $config;
		$this->rex_database     = rex_database::get_instance( $config );
		$this->mediaroot_length = strlen( $config["mediaroot"] );
	}

	function get( $parent, $id_start = null, $only_files = false ) {

		if ( ! is_null( $id_start ) ) {
			$id_start = intval( $id_start );
			if ( ! is_numeric( $id_start ) || $id_start < 1 ) {
				return false;
			}
		}

		if ( empty( $parent ) ) {
			$search_parent = "IS NULL";
		} else {
			$parent = intval( $parent );
			if ( ! is_numeric( $parent ) || $parent < 0 ) {
				return false;
			}
			$search_parent = "= '$parent'";
		}

		$search_extra = "";
		if ( $only_files ) {
			$search_extra = " AND `file` IS NOT NULL";
		}

		$sql    = "SELECT * FROM `" . $this->config["mysql_table_name_data"] . "` WHERE `parent` $search_parent$search_extra;";
		$result = rex_database::$db->query( $sql );

		$collect = [];

		if ( $result && isset( $result->num_rows ) && $result->num_rows > 0 ) {
			$hit = false;
			while ( $row = $result->fetch_assoc() ) {
				if ( is_null( $id_start ) ) {
					$collect[] = $row;
				} else {
					if ( $hit ) {
						$collect[] = $row;
						break;
					}
					if ( $row['id'] == $id_start ) {
						$hit = true;
					}
				}
			}

			return $collect;
		} else {
			return false;
		}
	}

	function render( $result_collect ) {

		$result = "";
		$script = "";

		$result_back    = "";
		$result_content = "";

		if ( empty( $result_collect ) ) {
			return "<div class='col-12'>Syntax Error, No data found!</div>\n";
		} else {
			$foo         = 0;
			$last_dir    = null;
			$back_hit    = false;
			$dir_counter = 0;
			$collect     = [];

			foreach ( $result_collect as $item ) {
				$has_childs = rex_database::has_childs( $item['id'] );

				if ( is_numeric( $item['parent'] ) && $item['parent'] > 0 && ! $back_hit ) {
					$back_hit    = true;
					$result_back .= "<div class='row element-back-link'>\n";
					$result_back .= "<div class='col-12'><span class='link back important button' id='href_back_$foo'>...Back</span></div>\n";
					$result_back .= "</div>\n";

					$parent_extra_wurst = rex_database::get_second_parent( $item['parent'] );

					if ( is_null( $parent_extra_wurst ) ) {
						$parent_extra_wurst = 0;
					}

					$script .= "$('#href_back_$foo').mousedown(function() { href_parent($parent_extra_wurst); });\n";
				}

				if ( $item['dir'] != $last_dir ) {
					if ( $has_childs ) {
						$sub_dir = true;
					} else {
						$sub_dir = false;
					}

					$dir_counter ++;
					$collect[$dir_counter]["dir"]   = [];
					$collect[$dir_counter]["files"] = [];
					$collect[$dir_counter]["dir"]   = [
						"name"    => substr( $item['dir'], $this->mediaroot_length ),
						"sub_dir" => $sub_dir,
						"id"      => $item['id'],
					];
				}
				$last_dir = $item['dir'];

				if ( ! $has_childs ) {
					$collect[$dir_counter]["files"][] = [ "name"   => $item['file'],
					                                      "played" => $item['played'],
					                                      "id"     => $item['id'],
					];
				}
			}
			$last_dir = null;
			foreach ( $collect as $value ) {
				$foo ++;

				$album = "";



				if ( isset( $value["dir"] ) ) {
					if ( $value["dir"]["sub_dir"] ) {
						$class_clickable = " subdir";
						$dir_class_extra = " clickable";
						$text_prefix     = "";
						$script          .= "$('#href_parent_$foo').mousedown(function() { href_parent(" . $value["dir"]['id'] . "); });\n";

						$dir = $value["dir"]['name'] . $text_prefix;

						$result_content .= "<div class='row element-subdir-link'><div class='col-sm-12'><div class='content_dir$class_clickable'><span id='href_parent_$foo' class='link dir$dir_class_extra'>$dir</span></div></div></div>";
						$last_dir = null;
					}

					if ( isset( $value["files"] ) && ! empty( ( $value["files"] ) ) ) {
						$class_clickable = "";
						$dir_class_extra = " shown";
						$text_prefix     = ":";

						$dir              = $value["dir"]['name'];
						$dir_short        = rex_helper::remove_last_dir_from_path( $dir );
						$dir_short_length = strlen( $dir_short );
						$album            = substr( $dir, $dir_short_length + 1 );

						$dir = $dir_short . $text_prefix;
						if($last_dir != $dir)
							$result_content .= "<div class='row element-dir-name'><div class='col-sm-12'><div class='content_dir$class_clickable'><span id='href_parent_$foo' class='link dir$dir_class_extra'>$dir</span></div></div></div>";
						$last_dir = $dir;

					}


				}

				if ( isset( $value["files"] ) && ! empty( ( $value["files"] ) ) ) {
					$result_content .= "<div class='row element-album'>";

					if ( ! empty( $album ) ) {
						$result_content .= "<div class='col-sm-3'><div class='album'><span>$album</span><div class='sun'></div></div></div>";
					}

					$result_content .= "<div class='col-sm-9'>";
					foreach ( $value["files"] as $file ) {
						if(empty($file['name']))
							continue;
						$foo ++;
						$result_content .= "<div class='row'><div class='col-sm-12'><div id='href_id_$foo' class='link file'><span class='played'>".$file['played'].":</span> " . $file['name'] . "</div></div></div>";
						$script         .= "$('#href_id_$foo').mousedown(function() { href_id(" . $file['id'] . "); });\n";
					}
					$result_content .= "</div>";

					$result_content .= "</div>";
				}
			}

			return "$result_back\n$result_content\n<script>\n$script\n</script>";
		}
	}

	function set_playlist_session( $parent, $id_start, $session_name = "playlist" ) {

		if ( ! isset( $_SESSION[$session_name] ) ) {
			$_SESSION[$session_name] = [];
		}

		$_SESSION[$session_name . "_debug"] = [ $parent, $id_start, $session_name ];
		$_SESSION[$session_name]            = $this->get( $parent, $id_start, true );
	}

}

class rex_collect {

	private $config;

	private $rex_database;

	function __construct( $config ) {

		$this->config       = $config;
		$this->rex_database = rex_database::get_instance( $config );
	}

	function start() {

		$collection = rex_helper::get_dir_content_recursive( $this->config["mediaroot"], $this->config["filetypes"] );
		$this->db_clean();
		$this->db_insert( $collection );
	}

	function db_clean() {

		rex_database::clean();
	}

	function db_insert( $collection ) {

		if ( ! is_array( $collection ) || empty( $collection ) ) {
			return false;
		}

		foreach ( $collection as $path ) {
			rex_database::insert_file( rex_helper::get_path( $path ), rex_helper::get_file( $path ) );
		}
	}

}

class rex_database {

	static $db;

	static $instance = null;

	static $config;

	static $not_allowed_file = [ ".", ".." ];
	static $not_allowed_dir = [ ".", ".." ];

	static function get_instance( $config ) {

		if ( is_null( static::$instance ) ) {
			static::$instance = new rex_database( $config );
		}

		return static::$instance;
	}

	function __construct( $config ) {

		static::$config = $config;
		static::$db     = $this->connect();
		$this->init_table();
	}

	function connect() {

		$db = new mysqli ( static::$config["mysql_host"], static::$config["mysql_database_user"], static::$config["mysql_database_password"], static::$config["mysql_database"], static::$config["mysql_port"] );

		$db->set_charset( "utf8" );
		$db->query( "SET NAMES 'utf8'" );
		$db->query( "SET CHARACTER SET 'utf8'" );

		return $db;
	}

	private function table_exist() {

		$sql = "SHOW TABLES LIKE '" . static::$config["mysql_table_name_data"] . "';";

		$result = static::$db->query( $sql );

		if ( $result && isset( $result->num_rows ) && $result->num_rows === 1 ) {
			return true;
		} else {
			return false;
		}
	}

	private function init_table() {

		if ( $this->table_exist() ) {
			return true;
		}

		$sql = "CREATE TABLE `" . static::$config["mysql_database"] . "`.`" . static::$config["mysql_table_name_data"] . "` ( 
        `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT , 
        `parent` BIGINT UNSIGNED NULL DEFAULT NULL , 
        `dir` VARCHAR(4096) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , 
        `file` VARCHAR(4096) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL , 
        `played` INT UNSIGNED NOT NULL DEFAULT '0' , 
        PRIMARY KEY (`id`)) ENGINE = InnoDB;";

		static::$db->query( $sql );
	}

	static function insert_file( $path, $file ) {

		if ( empty( $path ) || in_array( $file, static::$not_allowed_file ) ) {
			return false;
		}

		if ( static::file_exists( $path, $file ) ) {
			return false;
		}

		$parent = static::get_parent( $path );

		if ( $parent === false ) {
			return false;
		} elseif ( empty( $parent ) ) {
			$parent = "null";
		} else {
			$parent = "'$parent'";
		}

		if ( $file === false ) {
			$file = "null";
		} else {
			$file = "'$file'";
		}

		$sql = "INSERT INTO `" . static::$config["mysql_table_name_data"] . "` (`parent`, `dir`, `file`) VALUES ($parent, '$path', $file);";
		static::$db->query( $sql );
	}

	static function get_path( $id, $return_type = "string" ) {

		$id = intval( $id );
		if ( ! is_numeric( $id ) || $id < 1 ) {
			return false;
		}

		$path   = false;
		$sql    = "SELECT `dir`,`file` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `id` = '$id' LIMIT 1;";
		$result = static::$db->query( $sql );
		if ( $result && isset( $result->num_rows ) && $result->num_rows === 1 ) {
			$row = $result->fetch_assoc();
			if ( ! empty( $row['dir'] ) && ! empty( $row['file'] ) ) {
				$path = $row['dir'] . DIRECTORY_SEPARATOR . $row['file'];
				if ( ! is_file( $path ) ) {
					$path = false;
				} else {
					switch ( $return_type ) {
						case "array":
							$size = rex_helper::formatSizeUnits( filesize( $path ) );

							return [
								"path"                => $path,
								"dir"                 => $row['dir'],
								"file"                => $row["file"],
								"directory_separator" => DIRECTORY_SEPARATOR,
								"size"                => $size,
							];
							break;
						default:
							return $path;
							break;
					}
				}
			}
		}

		return $path;
	}

	static function get_parent( $path ) {

		if ( ! is_numeric( $path ) && is_string( $path ) ) {
			$dir_parent = rex_helper::remove_last_dir_from_path( $path );

			if ( $dir_parent === false ) {
				return false;
			}
		} elseif ( is_numeric( $path ) ) {
			$id_parent = intval( $path );
			if ( ! ( $id_parent && $id_parent > - 1 ) ) {
				return false;
			}
			$dir_parent = true; //
		} else {
			return false;
		}

		$row_parent = 0;

		if ( ! empty( $dir_parent ) ) {
			if ( ! is_numeric( $path ) && is_string( $path ) ) {
				$sql = "SELECT `id` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `dir` LIKE BINARY '$dir_parent' AND `file` IS NULL ORDER BY `id` LIMIT 1;";
			} else {
				$sql = "SELECT `parent` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `id` = '$id_parent' ORDER BY `id` LIMIT 1;";
			}
			$result = static::$db->query( $sql );
			if ( $result && isset( $result->num_rows ) && $result->num_rows === 1 ) {
				$row = $result->fetch_assoc();
				if ( ! is_numeric( $path ) && is_string( $path ) ) {
					$row_parent = $row['id'];
				} else {
					$row_parent = $row['parent'];
				}
			}
		}

		return $row_parent;
	}

	static function file_exists( $path, $file ) {

		if ( $file === false ) {
			$search_file = "`file` IS NULL";
		} else {
			$search_file = "`file` LIKE BINARY '$file'";
		}
		$sql = "SELECT `id` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE $search_file AND `dir` LIKE BINARY '$path' LIMIT 1;";

		$result = static::$db->query( $sql );
		if ( $result && isset( $result->num_rows ) && $result->num_rows == 1 ) {
			$row = $result->fetch_assoc();

			return $row['id'];
		} elseif ( $result && isset( $result->num_rows ) && $result->num_rows > 1 ) {
			return "error to many results";  // Todo: Catch
		} else {
			return false;
		}
	}

	static function clean() {

		$mediaroot_short = substr( static::$config["mediaroot"], 0, - 1 );

		$sql = "DELETE FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `dir` NOT LIKE '" . $mediaroot_short . "%';";
		static::$db->query( $sql );

		if ( is_array( static::$config["filetypes"] ) ) {
			$filetype_search_array = [];
			foreach ( static::$config["filetypes"] as $filetype ) {
				$filetype_search_array[] = "`file` NOT LIKE '%" . $filetype . "'";
			}
			$filetype_search = [];
			$filetype_search = implode( " AND ", $filetype_search_array );
		} else {
			$filetype_search = "`file` NOT LIKE '%" . static::$config["filetypes"] . "'";
		}

		$sql = "DELETE FROM `" . static::$config["mysql_table_name_data"] . "` WHERE $filetype_search;";
		static::$db->query( $sql );

		$sql = "DELETE FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `dir` NOT LIKE '" . $mediaroot_short . "%';";
		static::$db->query( $sql );

		$sql    = "SELECT * FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `file` IS NOT NULL;";
		$result = static::$db->query( $sql );
		while ( $row = $result->fetch_assoc() ) {
			if ( ! file_exists( $row['dir'] . DIRECTORY_SEPARATOR . $row['file'] ) ) {
				static::delete( $row['id'] );
			}
		}

		$sql    = "SELECT * FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `file` IS NULL;";
		$result = static::$db->query( $sql );
		while ( $row = $result->fetch_assoc() ) {
			if ( ! is_dir( $row['dir'] . DIRECTORY_SEPARATOR . $row['file'] ) ) {
				static::delete( $row['id'] );
			}
		}
	}

	static function delete( $id ) {

		$sql = "DELETE FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `id` = '$id' LIMIT 1";
		static::$db->query( $sql );
	}

	static function has_childs( $id ) {

		$id = intval( $id );
		if ( ! is_numeric( $id ) || $id < 1 ) {
			return false;
		}
		$sql    = "SELECT `parent` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `parent` = '$id';";
		$result = static::$db->query( $sql );
		if ( $result && isset( $result->num_rows ) && $result->num_rows > 0 ) {
			return true;
		} else {
			return false;
		}
	}

	static function get_second_parent( $parent ) {

		$parent = intval( $parent );
		if ( ! is_numeric( $parent ) || $parent < 1 ) {
			return 0;
		}
		$sql    = "SELECT `parent` FROM `" . static::$config["mysql_table_name_data"] . "` WHERE `id` = '$parent';";
		$result = static::$db->query( $sql );
		if ( $result && isset( $result->num_rows ) && $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();

			return $row['parent'];
		} else {
			return 0;
		}
	}

	static function rise_value( $id, $row = "played" ) {

		$id = intval( $id );
		if ( ! is_numeric( $id ) || $id < 1 ) {
			return false;
		}
		$sql = "UPDATE `" . static::$config["mysql_table_name_data"] . "` set `played` = `played` + 1 WHERE `id` = '$id' LIMIT 1";
		static::$db->query( $sql );
	}
}

class rex_helper {

	static function remove_last_dir_from_path( $path ) {

		$result = dirname( $path );
		if ( $result == "." ) {
			return "";
		}

		if ( in_array( $result, rex_database::$not_allowed_dir ) ) {
			return false;
		}

		return $result;
	}

	static function get_dir_content_sorted( $path ) {

		// Make sure we have a trailing slash and asterix
		$path = rtrim( $path, '/' ) . '/*';

		$dirs = glob( $path, GLOB_ONLYDIR );

		$files = glob( $path );

		return array_unique( array_merge( $dirs, $files ) );
	}

	static function get_dir_content_recursive( $dir, $extension = "mp3", &$results = [] ) {

		$files = static::get_dir_content_sorted( $dir );

		foreach ( $files as $value ) {

		    $path = realpath( $value );

			if ( ! is_dir( $path ) ) {
				if ( empty( $extension ) ) {
					$results[] = $path;
				} elseif ( ! is_array( $extension ) ) {
					if ( strcasecmp( static::get_filetype( $path ), $extension ) ) {
						$results[] = $path;
					}
				} elseif ( is_array( $extension ) ) {
					if ( in_array( strtolower( static::get_filetype( $path ) ), $extension ) ) {
						$results[] = $path;
					}
				}
			} else {
				$results[] = $path;
				static::get_dir_content_recursive( $path, $extension, $results );
			}
		}

		return $results;
	}

	static function get_filetype( $path ) {

		return pathinfo( $path, PATHINFO_EXTENSION );
	}

	static function get_path( $path ) {

		if ( is_dir( $path ) ) {
			return $path;
		}

		return pathinfo( $path, PATHINFO_DIRNAME );
	}

	static function get_file( $path ) {

		if ( strpos( $path, "." ) === false || is_dir( $path ) ) {
			return false;
		}

		return pathinfo( $path, PATHINFO_BASENAME );
	}

	static function formatSizeUnits( $bytes ) {

		if ( $bytes >= 1073741824 ) {
			$bytes = number_format( $bytes / 1073741824, 2 ) . ' GB';
		} elseif ( $bytes >= 1048576 ) {
			$bytes = number_format( $bytes / 1048576, 2 ) . ' MB';
		} elseif ( $bytes >= 1024 ) {
			$bytes = number_format( $bytes / 1024, 2 ) . ' KB';
		} elseif ( $bytes > 1 ) {
			$bytes = $bytes . ' bytes';
		} elseif ( $bytes == 1 ) {
			$bytes = $bytes . ' byte';
		} else {
			$bytes = '0 bytes';
		}

		return $bytes;
	}

	static function find( $needle, $haystack ) {

		if ( is_array( $haystack ) ) {
			if ( in_array( strtolower( $needle ), $haystack ) ) {
				return true;
			} else {
				return false;
			}
		} elseif ( $needle == $haystack ) {
			return true;
		} else {
			return false;
		}
	}
}

/////////////////77///
/// GET/POST Actions
/// //////////////////

if ( isset( $_POST ) && ! empty( $_POST ) ) {
	if ( isset( $_POST['parent'] ) ) {
		$parent = intval( $_POST['parent'] );
		if ( ! is_numeric( $parent ) || $parent < 0 ) {
			$response = [ "error" => "Syntax Error, Parent ID malformed;" ];
		} else {
			$_SESSION["parent"] = $parent;
			$response           = [ "parent" => $parent ];
		}
	} elseif ( isset( $_POST['id'] ) ) {
		$id = intval( $_POST['id'] );
		if ( ! is_numeric( $id ) || $id < 0 ) {
			$response = [ "error" => "Syntax Error, ID malformed;" ];
		} else {
			$_SESSION["id"] = $id;

			rex_database::get_instance( $config );
			$parent = rex_database::get_parent( $id );

			$c_rex_browsemedia = new rex_browsemedia( $config );
			$c_rex_browsemedia->set_playlist_session( $parent, $id );

			$response = [ "id" => $id ];
		}
	}

	echo json_encode( $response );
}
