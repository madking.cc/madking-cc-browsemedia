<?php

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	define( "rex_browsemedia", 1 );
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );
require_once( $dir_root . "class.php" );

$content = "";
$id      = 0;

if ( isset( $_GET ) && ! empty( $_GET ) ) {
	if ( isset( $_GET['id'] ) ) {
		$id = intval( $_GET['id'] );
		if ( ! is_numeric( $id ) || $id < 1 ) {

		} else {

			//$_SESSION["current"];

			rex_database::get_instance( $config );
			$file_info      = rex_database::get_path( $id, "array" );
			if($file_info) {
				$path      = $file_info["path"];
				$mime_type = rex_stream::get_mime_type( $path );

				$content .= "<div class='wrapper-audio'>
<div id='element-audio'><audio autoplay id=\"player\" src=\"stream.php?id=$id\" controls preload=\"auto\"></audio></div> 
<div id='element-current-title'><span class='info'>>> ".$file_info["file"]." (".$file_info["size"].") &lt;&lt;</span></div>
<div id='element-button'><span id='next' class='button'>Next</span></div>
</div>";
				rex_database::rise_value( $id );
			}
			$parent            = rex_database::get_parent( $id );
			$c_rex_browsemedia = new rex_browsemedia( $config );
			$c_rex_browsemedia->set_playlist_session( $parent, $id );
		}
	}
}

$content .= "
<script>

function audiochangestream () {
  var request = $.ajax({
    url: 'playlist.php',
    method: 'POST',
    dataType: 'json'
  })

  request.done(function (msg) {
    
    if (msg.hasOwnProperty('error')) {
      alert(msg['error'])
    }
    if (msg.hasOwnProperty('dataset')) {
      nextid = msg['dataset']['id'];
      var player = document.getElementById(\"player\");	
      player.addEventListener(\"ended\",function() {
          window.location.href = 'player.php?id=' + msg['dataset']['id'];
      });
    }
    //if (msg.hasOwnProperty('parent')) {
      //window.location.reload(false);
    //}
    //if (msg.hasOwnProperty('notice')) {
    //   alert(msg['notice'])
    //}
  })
  request.fail(function (jqXHR, textStatus) {
    alert('Request failed: ' + textStatus)
  })
}
audiochangestream();

$( document ).ready(function() {
 
  
  	$('#next').mousedown(function() {if (typeof nextid !== 'undefined')window.location.href = 'player.php?id=' + nextid;});
  
});

</script>
";

$template_type = "full";
require_once( $dir_root . "template-elements.php" );

echo $content_header . "
<body>
$content
" . $content_footer;
