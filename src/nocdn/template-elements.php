<?php
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

if ( ! defined( "rex_browsemedia" ) ) {
	die();
}

$dir_root = __DIR__ . "/";
require_once( $dir_root . "config.php" );
require_once( $dir_root . "debug.php" );

if ( ! isset( $template_type ) ) {
	$template_type = "full";
}

switch ( $template_type ) {

	///////////
	/// /jquery ONLY
	/// //////////

	case "jquery":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">

<title>Player</title>

<script src=\"./jquery-3.3.1.min.js\"></script>
<link rel=\"stylesheet\" href=\"./style.css\">

<style>
	body { background: #010101; color: #fefefe; font-size: 120%; }
</style>
  </head>
";

		$content_footer = "</body></html>";
		break;




	////////////77
	/// / NOCODE
	/// ////////////

	case "nocode":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">
<link rel=\"icon\" type=\"image/png\" href=\"./favicon.png\" />
<title>rex-browsemedia</title>
<link rel=\"stylesheet\" href=\"./style.css\">
  </head>
";

		$content_footer = "</body></html>";
		break;

	/////////////77
	/// ///////FULL
	/// //////////////

	case "full":
		$content_header = "<!doctype html>
<html lang=\"en\">
  <head>
    <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">
<meta name=\"description\" content=\"\">
<meta name=\"author\" content=\"Andreas Rex\">
<meta name=\"generator\" content=\"\">

<title>Browser</title>

<link rel=\"stylesheet\" href=\"./bootstrap.min.css\">
<script src=\"./jquery-3.3.1.min.js\"></script>
<script src=\"./popper.min.js\"></script>
<script src=\"./bootstrap.min.js\"></script>

<script src=\"./ajax.js\"></script>  
<link rel=\"stylesheet\" href=\"./style.css\">
  </head>
";

		$content_footer = "</body></html>";
		break;
	default:
		$content_header = "";
		$content_footer = "";
		break;
}
